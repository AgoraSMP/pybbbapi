use pyo3::prelude::*;
use bbb_api::BbbApi as NativeBbbApi;
use tokio::runtime::Runtime;

#[pyclass]
struct BbbApi{ inner : NativeBbbApi, runtime : Runtime}

#[pymethods]
impl BbbApi {
	#[new]
	fn new(bbb_api_url : String, bbb_api_secret : String) -> Self {
		Self { inner : NativeBbbApi {
			bbb_api_secret,
			bbb_api_url,
			bot_nickname : "unimplemented".into()
		}, runtime : tokio::runtime::Runtime::new().unwrap()}
	}
	
	fn build_url(&self, endpoint: &str, params : &pyo3::types::PyDict) -> Result<String, PyErr> {
		let mut owned : Vec<(String, String)> = Vec::with_capacity(params.len());
		for (key, value) in params.iter() {
			owned.push((key.to_string(), value.to_string()));
		}
		let convert : Vec<(&str, &str)> = owned.iter().map(|(k,v)| (k.as_ref(), v.as_ref())).collect();
		Ok(self.inner.build_url(endpoint, &convert))
	}
	
	fn end_meeting(&mut self, meeting_id : String) -> Result<(), PyErr> {
		self.runtime.block_on(self.inner.end_meeting(&meeting_id)).map_err(|e| pyo3::exceptions::IOError::py_err(format!("{:?}", e)))
	}
}

#[pymodule]
fn pybbbapi(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<BbbApi>()?;
    Ok(())
}
